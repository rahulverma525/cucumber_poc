@google
Feature: Google Search

@search
Scenario: Searching Google

  Given I open Google's search page
  Then the title is "Google"
  And the Google search form exists

 @searchagain
Scenario: Searching Google again

  Given I open Google's search page again
  Then the title is again "Google" 
  And the Google search form exists again
